# Cheatsheet

This project is a collection of tools related to the field of cybersecurity. This project relies on [Mitre's tactics](https://attack.mitre.org/tactics/enterprise/) to categorize each of the detailed tools.

## Index

* **Reconnaissance** (TA0043)
* **Initial Access** (TA0001)
* **Execution** (TA0002)
* **Defense Evasion** (TA0005)
* **Reconnaissance** (TA0043)
* **Collection** (TA0009)
* **Privilege Escalation** (TA0004)
* **Persistence** (TA0003)
* **Credential Access** (TA0006)
* **Lateral Movement** (TA0008)
* **Exfiltration** (TA0010)
* **Command and Control** (TA0011)

## Tools

### Reconnaissance

* https://github.com/robertdavidgraham/masscan
* https://github.com/nmap/nmap
* https://github.com/takeshixx/nmap-scripts
* https://github.com/cddmp/enum4linux-ng
* https://github.com/Zer1t0/ntlm-info


### Initial Access

* https://github.com/kgretzky/evilginx2
* https://github.com/JohnWoodman/VBA-Macro-Reverse-Shell
* https://github.com/itm4n/VBA-RunPE


### Execution

* https://github.com/nccgroup/demiguise


### Defense Evasion

* https://github.com/Hackplayers/Salsa-tools/
* https://github.com/TheWover/donut
* https://github.com/FuzzySecurity/Sharp-Suite
* https://github.com/rasta-mouse/RuralBishop
* https://github.com/cobbr/SharpSploit/tree/master/SharpSploit/Execution/DynamicInvoke
* https://github.com/forrest-orr/phantom-dll-hollower-poc
* https://github.com/hasherezade/module_overloading
* https://github.com/BorjaMerino/Pazuzu
* https://github.com/secretsquirrel/SigThief
* https://github.com/phra/PEzor
* https://github.com/rasta-mouse/AmsiScanBufferBypass
* https://github.com/rasta-mouse/TikiTorch
* https://github.com/rasta-mouse/DInvoke
* https://github.com/danielbohannon/Invoke-Obfuscation
* https://github.com/monoxgas/sRDI
* https://github.com/med0x2e/GadgetToJScript
* https://github.com/Flangvik/NetLoader
* https://github.com/med0x2e/NoAmci
* https://github.com/jthuraisamy/SysWhispers
* https://github.com/bohops/GhostBuild
* https://github.com/outflanknl/SharpHide
* https://github.com/Aetsu/OffensivePipeline
* https://github.com/optiv/ScareCrow
* https://github.com/jxy-s/herpaderping/
* https://gist.github.com/rxwx/c5e0e5bba8c272eb6daa587115ae0014#file-uuid-c
* https://github.com/matterpreter/SHAPESHIFTER
* https://github.com/xforcered/Dendrobate


### Discovery

* https://github.com/GhostPack/Seatbelt
* https://github.com/BloodHoundAD/SharpHound3
* https://github.com/BloodHoundAD/BloodHound
* https://github.com/fox-it/BloodHound.py
* https://github.com/PowerShellMafia/PowerSploit/blob/dev/Recon/PowerView.ps1
* https://github.com/tevora-threat/SharpView
* https://github.com/phra/Recon-AD
* https://github.com/vletoux/pingcastle/
* https://github.com/rvrsh3ll/SharpPrinter
* https://github.com/HunnicCyber/SharpSniper
* https://github.com/djhohnstein/SharpShares
* https://github.com/l0ss/Grouper2
* https://github.com/rvrsh3ll/SharpSSDP
* https://github.com/FSecureLABS/SharpGPO-RemoteAccessPolicies


### Collection

* https://github.com/SnaffCon/Snaffler
* https://github.com/slyd0g/SharpClipboard
* https://github.com/djhohnstein/WireTap
* https://github.com/vivami/SauronEye


### Privilege Escalation

* https://github.com/rasta-mouse/Watson
* https://github.com/PowerShellMafia/PowerSploit/blob/dev/Privesc/PowerUp.ps1
* https://github.com/GhostPack/SharpUp
* https://github.com/ohpe/juicy-potato
* https://github.com/foxglovesec/RottenPotato
* https://github.com/breenmachine/RottenPotatoNG
* https://github.com/hfiref0x/UACME
* https://github.com/Hackplayers/PsCabesha-tools/tree/master/Privesc
* https://github.com/BeichenDream/BadPotato
* https://github.com/itm4n/PrivescCheck
* https://github.com/itm4n/PrintSpoofer
* https://github.com/itm4n/UsoDllLoader
* https://github.com/itm4n/FullPowers
* https://github.com/itm4n/Perfusion
* https://github.com/itm4n/BitsArbitraryFileMove


### Persistence

* https://github.com/fireeye/SharPersist
* https://github.com/0xthirteen/SharpStay
* https://github.com/ewhitehats/InvisiblePersistence
* https://medium.com/@two06/persistence-with-keepass-part-1-d2e705326aa6
* https://pentestlab.blog/2019/10/09/persistence-screensaver/
* https://pentestlab.blog/2019/10/29/persistence-netsh-helper-dll/
* https://pentestlab.blog/2020/01/13/persistence-image-file-execution-options-injection/
* https://pentestlab.blog/2019/12/11/persistence-office-application-startup/
* https://pentestlab.blog/2020/05/20/persistence-com-hijacking/
* https://www.hexacorn.com/blog/?s=beyond+good
* https://github.com/rootm0s/WinPwnage#persistence-techniques
* https://github.com/swisskyrepo/PayloadsAllTheThings/blob/master/Methodology%20and%20Resources/Windows%20-%20Persistence.md


### Credential Access

* https://github.com/gentilkiwi/mimikatz
* https://github.com/b4rtik/SharpKatz
* https://github.com/GhostPack/SafetyKatz
* https://github.com/Flangvik/BetterSafetyKatz
* https://github.com/GhostPack/Rubeus
* https://github.com/eladshamir/Internal-Monologue
* https://github.com/milkdevil/incognito2
* https://github.com/GhostPack/KeeThief
* https://github.com/GhostPack/SharpDPAPI
* https://github.com/GhostPack/SharpDump
* https://github.com/oxfemale/LogonCredentialsSteal
* https://github.com/Pickfordmatt/SharpLocker
* https://github.com/EncodeGroup/Gopher
* https://github.com/jnqpblc/SharpSpray
* https://github.com/chrismaddalena/SharpCloud
* https://github.com/swisskyrepo/SharpLAPS
* https://github.com/PorLaCola25/TransactedSharpMiniDump
* https://github.com/S4R1N/Tritium


### Lateral Movement

* https://github.com/blackarrowsec/pivotnacci
* https://github.com/jpillora/chisel
* https://github.com/shantanu561993/SharpChisel
* https://github.com/rasta-mouse/AsyncNamedPipes
* https://github.com/Hackplayers/evil-winrm
* https://github.com/rasta-mouse/MiscTools
* https://github.com/0xthirteen/SharpRDP
* https://github.com/lgandx/Responder
* https://github.com/NetSPI/PowerUpSQL
* https://github.com/GhostPack/Rubeus
* https://github.com/gentilkiwi/mimikatz
* https://github.com/0xthirteen/SharpMove
* https://github.com/malcomvetter/CSExec
* https://github.com/p3nt4/Invoke-SocksProxy
* https://github.com/PowerShellMafia/PowerSploit/blob/dev/Recon/PowerView.ps1
* https://gist.github.com/G0ldenGunSec/80a00087cd10336c6954a2c955746ba9
* https://github.com/cube0x0/SharpMapExec
* https://github.com/FSecureLABS/SharpGPOAbuse
* https://github.com/pkb1s/SharpAllowedToAct
* https://github.com/FatRodzianko/Get-RBCD-Threaded
* https://gist.github.com/TarlogicSecurity/2f221924fef8c14a1d8e29f3cb5c5c4a


### Exfiltration

* https://github.com/IncideDigital/Mistica
* https://github.com/iagox86/dnscat2
* https://github.com/lukebaggett/dnscat2-powershell
* https://github.com/3v4Si0N/HTTP-revshell
* https://github.com/hadur-borzsei-kallo/SharpHungarian


### Command and Control

* https://github.com/cobbr/Covenant
* https://github.com/BC-SECURITY/Empire
* https://github.com/BishopFox/sliver
